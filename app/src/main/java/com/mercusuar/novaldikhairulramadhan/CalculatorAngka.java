package com.mercusuar.novaldikhairulramadhan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculatorAngka extends AppCompatActivity {
    //Buat variabel
    EditText angkaSatu, angkaDua;
    Button jumlah, kurang, kali, bagi, bersih;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_angka);
        //Ambil nilai varoabel
        angkaSatu = (EditText) findViewById(R.id.editTextNumber);
        angkaDua = (EditText) findViewById(R.id.editTextNumber2);
        jumlah = (Button) findViewById(R.id.btnTambah);
        kurang = (Button) findViewById(R.id.btnKurang);
        kali = (Button) findViewById(R.id.btnKali);
        bagi = (Button) findViewById(R.id.btnBagi);
        bersih= (Button) findViewById(R.id.btnBersih);
        hasil = (TextView) findViewById(R.id.editTextNumberHasil);
        jumlah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (angkaSatu.length() == 0 && angkaDua.length() == 0) {
                    Toast.makeText(getApplication(),"Angka Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }
                else if (angkaSatu.length() == 0) {
                    Toast.makeText(getApplication(),"Angka 1 Boleh Kosong", Toast.LENGTH_SHORT).show();
                }
                else if (angkaDua.length() == 0) {
                    Toast.makeText(getApplication(),"Angka 2 Boleh Kosong", Toast.LENGTH_SHORT).show();
                } else {
                    String isiAngka1 = angkaSatu.getText().toString();
                    String isiAngka2 = angkaDua.getText().toString();
                    //parsing isi menjadi double
                    double A1 = Double.parseDouble(isiAngka1);
                    double A2 = Double.parseDouble(isiAngka2);
                    //panggil method Jumlah
                    double hs = hitungJumlah(A1, A2);
                    //parsing hasil menjadi string
                    String output = String.valueOf(hs);
                    //set txtHasil dengan Output (string hasil parsing)
                    hasil.setText(output.toString());
                }
            }
        });
        kurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput()) {
                    double A1 = Double.parseDouble(angkaSatu.getText().toString());
                    double A2 = Double.parseDouble(angkaDua.getText().toString());
                    double hs = hitungKurang(A1, A2);
                    hasil.setText(String.valueOf(hs));
                }
            }
        });

        // Tombol perkalian
        kali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput()) {
                    double A1 = Double.parseDouble(angkaSatu.getText().toString());
                    double A2 = Double.parseDouble(angkaDua.getText().toString());
                    double hs = hitungKali(A1, A2);
                    hasil.setText(String.valueOf(hs));
                }
            }
        });

        // Tombol Bagi
        bagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput()) {
                    double A1 = Double.parseDouble(angkaSatu.getText().toString());
                    double A2 = Double.parseDouble(angkaDua.getText().toString());
                    if (A2 == 0) {
                        Toast.makeText(getApplicationContext(), "Pembagian dengan nol tidak diizinkan", Toast.LENGTH_SHORT).show();
                    } else {
                        double hs = hitungBagi(A1, A2);
                        hasil.setText(String.valueOf(hs));
                    }
                }
            }
        });

        // Tombol Bersihkan
        bersih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angkaSatu.setText("");
                angkaDua.setText("");
                hasil.setText("");
                angkaSatu.requestFocus();
            }
        });
    }
    public double hitungJumlah(double Angka1, double Angka2) {
        return Angka1 + Angka2;
    }

    public double hitungKurang(double Angka1, double Angka2) {
        return Angka1 - Angka2;
    }

    public double hitungKali(double Angka1, double Angka2) {
        return Angka1 * Angka2;
    }

    public double hitungBagi(double Angka1, double Angka2) {
        return Angka1 / Angka2;
    }

    // Validasi input (cek apakah angkaSatu dan angkaDua tidak kosong)
    private boolean validateInput() {
        if (angkaSatu.length() == 0 || angkaDua.length() == 0) {
            Toast.makeText(getApplicationContext(), "Angka Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}