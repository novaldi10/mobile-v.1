package com.mercusuar.novaldikhairulramadhan;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import com.google.android.material.tabs.TabLayout;


public class menuProgram extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_program);
        Button menu=(Button) findViewById(R.id.button);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ScrollingActivity.class);
                startActivity(i);
            }
        });

        Button btnProg2=(Button) findViewById(R.id.button2);
        //button program 2
        btnProg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), tampilanWeb.class);
            startActivity(i);
            }
        });

        Button btnProg3=(Button) findViewById(R.id.button3);
        //button program 2
        btnProg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CalculatorAngka.class);
                startActivity(i);
            }
        });

        Button profile=(Button) findViewById(R.id.buttonProfil);
        //button profile
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), TabView.class);
                startActivity(i);
            }
        });

        Button media=(Button) findViewById(R.id.buttonMedia);
        //button profile
        media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Follower.class);
                startActivity(i);
            }
        });

        Button musik=(Button) findViewById(R.id.buttonMusik);
        //button profile
        musik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Musik.class);
                startActivity(i);
            }
        });

        Button game=(Button) findViewById(R.id.buttonGame);
        //button profile
        game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FlapyBird.class);
                startActivity(i);
            }
        });
    }
}