package com.mercusuar.novaldikhairulramadhan;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

public class Musik extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;
    private TextView textViewSongTitle;
    private Button playPauseButton;

    private List<Integer> songList;
    private int currentSongIndex = 0;

    private Button btnPrevious;
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musik);

        mediaPlayer = MediaPlayer.create(this, R.raw.mysong1); // Default song
        textViewSongTitle = findViewById(R.id.textViewSongTitle);
        seekBar = findViewById(R.id.seekBar);
        playPauseButton = findViewById(R.id.playPauseButton);

        songList = new ArrayList<>();
        songList.add(R.raw.mysong1);
        songList.add(R.raw.mysong2);
        songList.add(R.raw.mysong3);
        // Add more songs as needed

        textViewSongTitle.setText("My Song Title");

        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    playPauseButton.setText("Play");
                } else {
                    mediaPlayer.start();
                    playPauseButton.setText("Pause");
                    updateSeekBar();
                }
            }
        });

        btnPrevious = findViewById(R.id.prevButton);
        btnNext = findViewById(R.id.nextButton);

        // Set fungsi onClick untuk tombol previous dan next

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNextSong();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playNextSong();
            }
        });

        seekBar.setMax(mediaPlayer.getDuration());
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void updateSeekBar() {
        seekBar.setProgress(mediaPlayer.getCurrentPosition());
        if (mediaPlayer.isPlaying()) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    updateSeekBar();
                }
            };
            seekBar.postDelayed(runnable, 1000);
        }
    }

    private void playNextSong() {
        currentSongIndex = (currentSongIndex + 1) % songList.size();
        mediaPlayer.release();
        mediaPlayer = MediaPlayer.create(this, songList.get(currentSongIndex));
        textViewSongTitle.setText("My Song Title");
        mediaPlayer.start();
        updateSeekBar();
    }

    private void playPreviousSong() {
        currentSongIndex = (currentSongIndex - 1) % songList.size();
        if (currentSongIndex < 0) {
            currentSongIndex = songList.size() - 1;
        }
        mediaPlayer.release();
        mediaPlayer = MediaPlayer.create(this, songList.get(currentSongIndex));
        textViewSongTitle.setText("My Song Title");
        mediaPlayer.start();
        updateSeekBar();
    }

    public void onPreviousButtonClick(View view) {
        playPreviousSong();
    }

    public void onNextButtonClick(View view) {
        playNextSong();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
    }
}