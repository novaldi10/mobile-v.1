package com.mercusuar.novaldikhairulramadhan;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;


public class Login extends AppCompatActivity {
    Button b1,b2;
    EditText ed1,ed2;
    TextView tx1;
    int counter = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        b1 = (Button) findViewById(R.id.btnLogin);
        ed1 = (EditText) findViewById(R.id.editUsername);
        ed2 = (EditText) findViewById(R.id.editTextTextPassword);

        b2 = (Button) findViewById(R.id.btnExit);
        tx1 = (TextView) findViewById(R.id.textView9);
        tx1.setVisibility(View.GONE);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed1.getText().toString().equals("novaldi") &&
                    ed2.getText().toString().equals("novaldi")) {
                    Toast.makeText(getApplicationContext(),
                            "Berhasil...", Toast.LENGTH_SHORT).show();
                    // Alihkan pengguna ke tampilan menu program
                    Intent i = new Intent(Login.this, menuProgram.class);
                    startActivity(i);
                }
                else {
                    Toast.makeText(getApplicationContext(),"Oops, sepertinya kucing nakal mencoba masuk ke akun Anda. Coba lagi ya!",
                            Toast.LENGTH_SHORT).show();
                    tx1.setVisibility(View.VISIBLE);
                    tx1.setBackgroundColor(Color.RED);
                    counter--;
                    tx1.setText(Integer.toString(counter));

                    if (counter == 0) {
                        b1.setEnabled(false);
                    }

                }
            }
        });
        //b2.setOnClickListener(v -> finish());
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, Splash.class);
                startActivity(i);
            }
        });

    }
}